//Display home page
"use strict";

var React = require('react');
var Link = require('react-router').Link;
var UserActions = require('../../actions/userActions');
var UserStores = require('../../stores/userStore');

var UserProfile = React.createClass({

  getInitialState: function() {
    console.log('getInitialState currentuser == ' + JSON.stringify(UserStores.getCurrentUser()));
    console.log('getInitialState this.user == ' + JSON.stringify(this.user));

    return {
      user: UserStores.getCurrentUser()
    };
  },

	render: function() {
		return (
			<div className="jumbotron">
				<h1>Foogle</h1>
        <p>This is {this.props.params.userId} profile page</p>
			</div>
		);
	}
});

module.exports = UserProfile;
