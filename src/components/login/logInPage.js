"use strict";

var React = require('react');
var Router = require('react-router');
var LogInForm = require('./LogInForm');
var UserActions = require('../../actions/userActions');
var UserStores = require('../../stores/userStore');
var Parse = require('parse');
var ParseReact = require('parse-react');
var toastr = require('toastr');

var panelFooterStyle =  {
								margin: '0 auto',
								width: '40%'
						};

var LogInPage = React.createClass({

	mixins: [
    Router.Navigation
  ],

	getInitialState: function() {
    return {
      user: {userName: '', password: ''},
      errors: {},
    };
  },

	componentWillMount: function() {
    UserStores.addChangeListener(this._onchange);
  },

  componentWillUnMount: function() {
    UserStores.removeListener(this._onchange);
  },


	//Called whenever input changes in the sign up form
	handleInputChange: function(event){
    var field = event.target.name;
		var value = event.target.value;
	  this.state.user[field] = value;
	  return this.setState({user: this.state.user});
	},

	//Check user credentials
	formIsValid: function(userInfo) {
		var formIsValid = true;
		this.state.errors = {}; //clear any previous errors.

		if(this.state.user.userName.length < 3){
			this.state.errors.userName = 'First name must be at least 3 characters';
			formIsValid = false;
		}

		if(this.state.user.password.length < 3){
			this.state.errors.password = 'Password must be at least 3 characters';
			formIsValid = false;
		}

		this.setState({errors: this.state.errors});
		return formIsValid;
	},

	handleLogIn: function() {
		event.preventDefault();
		if(this.formIsValid()){
			Parse.User.logIn(this.state.user.userName, this.state.user.password, {
			  success: function(user) {
			    // Do stuff after successful login
					UserActions.logIn(user);
			  },
			  error: function(user, error) {
			    // The login failed. Check error to see why.
					alert("Error: " + error.code + " " + error.message);
			  }
			});		}
	},

	//Runs everytime store state changes
  _onchange: function() {
   //Redirect user to their profile
	 var currentUser = Parse.User.current();
   this.transitionTo('user/userName',
      {
       userId: Parse.User.current().get("username")
      });
   },

	render: function() {
		return (
			<div className="jumbotron">
				<LogInForm
					user={this.state.user}
					onChange={this.handleInputChange}
					onLogin={this.handleLogIn}
					errors={this.state.errors}/>
			</div>
		);
	}
});

module.exports = LogInPage;
