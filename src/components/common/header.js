"use strict";

var React = require('react');
var Link = require('react-router').Link;
var Parse = require('parse');
var ParseReact = require('parse-react');


var Header = React.createClass({
	render: function() {
	return (
		<nav className="navbar navbar-default navbar-inverse" role="navigation">
			<h5>
				<div className="container-fluid">
					<Link to="app" className="navbar-brand">
						<img src="./../images/logo_WasteNoFood_header.png"width="150" height="25"/>
						</Link>

				<div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul className="nav navbar-nav">
							<li><Link to="app">Home</Link>
							</li><li><Link to="about">About</Link>
							</li><li>&nbsp</li></ul>
						<ul className="nav navbar-nav navbar-right">
							<form className="navbar-form navbar-left" role="search">
								<div className="form-group">
									<input type="text" className="form-control" placeholder="Search Resturants"/>
								</div>
								<button type="submit" className="btn btn-default">Submit</button>
							</form>
							<li><Link to="login">Log in</Link></li>
						</ul>
					</div>
				</div>
			</h5>
		</nav>
	);
}
});

module.exports = Header;
