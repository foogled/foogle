//Display about page 
"use strict";

var React = require('react');

var About = React.createClass({

	render: function () {
		return (
			<div>
				<h1>About</h1>
				<p>This web application is a communication platform that connects the people who are in need for food with the people who have food to give</p>
				<div className="jumbotron">
					<p>
						<h4>This application uses the following technologies. 
						We will probably use the following development stack to create the site:</h4>
					</p>	
					<ul class="list-group">
						<li><a href="https://facebook.github.io/react/" class="list-group-item active">React - UI framework</a></li>
						<li><a href="https://github.com/rackt/react-router" class="list-group-item">React Router - client routing</a></li>
						<li><a href="https://facebook.github.io/flux/docs/overview.html#content" class="list-group-item">Flux - site architecture pattern</a></li>
						<li><a href="https://nodejs.org/en/docs/" class="list-group-item active">Node - server framework</a></li>
						<li><a href="https://github.com/gulpjs/gulp/blob/master/docs/API.md" class="list-group-item active">Gulp - tasks runner</a></li>
						<li><a href="https://github.com/substack/node-browserify#usage" class="list-group-item">Browserify - to bundle javascript files</a></li>
						<li><a href="http://getbootstrap.com/getting-started/" class="list-group-item">Bootstrap - for styling</a></li>
					</ul>
				</div>
			</div>
		);
	}
});

module.exports = About;

/**
	static methods
	willTransitionTo - Determine if page should be transitioned to
		- we can use it to validate users
	willTransitionFrom - Runs checks before user navigates a way
		- we can use it to check the forms data
**/