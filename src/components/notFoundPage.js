//404 error page 
"use strict";

var React = require('react');
var Link = require('react-router').Link;

var NotFoundPage = React.createClass({
	render: function() {
		return (
			<div className="jumbotron">
				<h1>Page Not Found</h1>
				<p>Whoops! Sorry, the page you requested is not found.</p>
				<Link to="app">Back to Home</Link>
			</div>
		);
	}
});

module.exports = NotFoundPage;