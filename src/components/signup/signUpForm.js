"use strict";

var React = require('react');
var TextInput = require('./../common/textInput');

var SignUpForm = React.createClass({
	render: function() {
		return (
			<div className="jumbotron">
			<div className="container">
					<div className="row vertical-offset-100" >
						<div className="col-md-8 col-md-offset-2">
								<div className="panel panel-default">

										<div className="panel-heading">
											<div className="row-fluid user-row">
												<img src="http://s11.postimg.org/7kzgji28v/logo_sm_2_mr_1.png"
											 className="img-responsive" alt="Conxole Admin"/>
											</div>
										</div>

										<div className="panel-body">
												<form accept-charset="UTF-8" role="form" className="form-signin">
														<fieldset>
															<TextInput
																name="userName"
																label="username"
																type="text"
																value={this.props.user.userName}
																onChange={this.props.onChange}
																error={this.props.errors.userName}/>
															<TextInput
																name="email"
																label="E-mail"
																type="email"
																value={this.props.user.email}
																onChange={this.props.onChange}
																error={this.props.errors.email}/>
															<TextInput
																name="password"
																label="password"
																type="password"
																value={this.props.user.password}
																onChange={this.props.onChange}
																error={this.props.errors.password}/>

															<br></br>
															<input className="btn btn-lg btn-success btn-block"
																type="submit"
																id="login"
																value="Sign Up"
																onClick={this.props.onSignUp}/>
													 </fieldset>
											 </form>
										</div>

										<div className="panel-footer">
												<h5>
													<br></br>
													<p><p></p></p>
												</h5>
										</div>
							 </div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = SignUpForm;
