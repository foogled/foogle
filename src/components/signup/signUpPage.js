//Display home page
"use strict";

var React = require('react');
var Router = require('react-router');
var SignUpForm = require('./signUpForm');
var UserActions = require('../../actions/userActions');
var UserStores = require('../../stores/userStore');
var Parse = require('parse');
var ParseReact = require('parse-react');
var toastr = require('toastr');


var SignUpPage = React.createClass({
  mixins: [
    Router.Navigation
  ],
  statics: {
    willTransitionFrom: function(transition, component) {
      if(component.state.dirty && !confirm('Leave without saving?')){
        transition.abort();
      }
    }
  },

  getInitialState: function() {
    return {
      user: {userName: '', email: '', password: ''},
      errors: {},
      dirty: false
    };
  },

  componentWillMount: function() {
    UserStores.addChangeListener(this._onchange);
  },

  componentWillUnMount: function() {
    UserStores.removeListener(this._onchange);
  },

  //Called whenever input changes in the sign up form
  handleInputChange: function(event){
    var field = event.target.name;
    var value = event.target.value;
    this.state.user[field] = value;
    this.setState({dirty: true});
    return this.setState({user: this.state.user});
  },

  //Check user credentials
  formIsValid: function(userInfo) {
    var formIsValid = true;
    this.state.errors = {}; //clear any previous errors.

    if(this.state.user.userName.length < 3){
      this.state.errors.userName = 'First name must be at least 3 characters';
      formIsValid = false;
    }

    //string.indexOf(substring
    if(this.state.user.email.indexOf('@') === -1){
      this.state.errors.email = 'Email must contains @';
      formIsValid = false;
    }

    if(this.state.user.password.length < 3){
      this.state.errors.password = 'Password must be at least 3 characters';
      formIsValid = false;
    }

    this.setState({errors: this.state.errors});
    return formIsValid;
  },

  saveUser: function(event){
    event.preventDefault();
    if(this.formIsValid()){
      this.setState({dirty: false});

      //Save user to parse
      this.saveUserInParse();
    }
  },

  saveUserInParse: function() {
    var user = new Parse.User();
    user.set("username", this.state.user.userName);
    user.set("password", this.state.user.password);
    user.set("email", this.state.user.email);

    user.signUp(null, {
      success: function(user) {
        //Fire up (SIGN_UP) action
        UserActions.signUp(user);
        toastr.success();
      },
      error: function(user, error) {
        // Show the error message somewhere and let the user try again.
        alert("Error: " + error.code + " " + error.message);
      }
    });
  },

  //Runs everytime store state changes
   _onchange: function() {
     //Redirect user to their profile
     var currentUser = Parse.User.current();
     this.transitionTo('user/userName',
       {
         userId: Parse.User.current().get("username")
       });
   },

	render: function() {
		return (
			<div className="jumbotron">
        <SignUpForm
          user={this.state.user}
          onChange={this.handleInputChange}
          onSignUp={this.saveUser}
          errors={this.state.errors}/>
			</div>
		);
	}
});

module.exports = SignUpPage;
