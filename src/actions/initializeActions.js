"use strict";
var Dispatcher = require('../dispatcher/appDispatcher');
var ActionTypes = require('../constants/actionTypes');

/* Initialize the application state here */
var InitializeActions = {
  //Action creator
  Init: function(loadData) {
    console.log('Action is called with type == ' + ActionTypes.INIT + '\n');
    Dispatcher.dispatch({
      actionType: ActionTypes.INIT,
      initData: loadData
    });
  }
};

module.exports = InitializeActions;
