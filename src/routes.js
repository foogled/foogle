"use strict";

var React = require('react');
var Router = require('react-router');
var DefaultRoute = Router.DefaultRoute;
var Route = Router.Route;
var NotFoundRoute = Router.NotFoundRoute;
var Redirect = Router.Redirect;

//Pages - components
var App = require('./components/app');
var HomePage = require('./components/homePage');
var AboutPage = require('./components/about/aboutPage');
var NotFoundPage = require('./components/notFoundPage');
var LogIn = require('./components/login/logInPage');
var SignUp = require('./components/signup/signUpPage');
var UserProfile = require('./components/userProfile/userProfile');

var routes = (
  <Route name="app" path="/" handler={App}>
    <DefaultRoute handler={HomePage} />
    <Route name="about" path="/about" handler={AboutPage} />
    <NotFoundRoute handler={NotFoundPage} />
    <Redirect from="about-us" to="about" />
    <Route name="login" path="/login" handler={LogIn} />
    <Route name="signup" path="/signup" handler={SignUp} />
    <Route name="user/userName" path="users/:userId" handler={UserProfile} />
  </Route>
);

module.exports = routes;

/*
	=======================  Information about how routing in react-router works ===============================

	//Given a route like this:
	<route path="/course/:courseId" handler={course} />

	// and a URL like this:
	'/course/clean-code?module=3'

	//The componenet props will be populated
	var course = React.createClass({
		render: function () {
			this.props.param.courseId //"clean-code"
			this.props.query.module;  // "3"
			this.props.path; //"/course/clean-code?module=3"
		}
	});

	//Links
	Route: <route name="user" path="/user/:userId" />
	JSX:   <link to="user" params={{userId: 1}}>Sample Text</link>

	Html:  <a href="/user/1">Sample Text</a>

	//Redirects
	Alias Redirect  	var Redirect = Router.Redirect;

	Create a new route  <Redirect from="old-path" to="name-of-new-path" />

  //Adding a page step
  1 - Add a file with the pageName
  2 - Add a route to the router.js
  3 - Add a way to navigate to that page
	===================================================================================================================

*/
