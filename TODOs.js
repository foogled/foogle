//Display about page
"use strict";

var React = require('react');

var About = React.createClass({

	render: function () {
		return (
			<div>
				<h1>About</h1>
				<p>This web application is a communication platform that connects the people who are in need for food with the people who have food to give</p>
				<div className="jumbotron">
					<p>
						<h4>This is a list of TODOs</h4>
					</p>
					<ul class="list-group">
						<li>Refactor login/signup pages so that they use the same panel</li>
					</ul>
				</div>
			</div>
		);
	}
});

module.exports = About;

/**
	static methods
	willTransitionTo - Determine if page should be transitioned to
		- we can use it to validate users
	willTransitionFrom - Runs checks before user navigates a way
		- we can use it to check the forms data
**/
